package com.test.chess.util;

public class ApplicationUtils {
	public static int convertCharToNum(char charIn) {
		int numOut = -1;
		for (int i = 0; i < ApplicationConstants.SIDE_ALPHABATES.length; i++) {

			if (ApplicationConstants.SIDE_ALPHABATES[i] == charIn) {
				numOut = i;
			}
		}
		return numOut;
	}

	public static int convertCharNumtoNum(char charIn) {
		int numOut = -1;
		int convertedNum = Character.getNumericValue(charIn);

		for (int i : ApplicationConstants.SIDE_NUMS) {
			if (i == convertedNum) {
				numOut = convertedNum;
			}
		}
		return numOut;
	}

	public static String getIcon(String color, String type) {
		StringBuilder builder = new StringBuilder();
		if (color == ApplicationConstants.WHITE) {
			builder.append("w");
		} else {
			builder.append("b");
		}
		return builder.append(type.substring(0, 1).toUpperCase()).append(type.substring(1, 2)).toString();
	}
}
