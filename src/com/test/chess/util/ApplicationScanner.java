package com.test.chess.util;

import java.util.Scanner;

import com.test.chess.board.Player;

public class ApplicationScanner {
	private static final Scanner scanner = new Scanner(System.in);

	public static String readPlayerName(String color) {
		String playerName = null;
		while (true) {
			System.out.print("For " + color + " color, Please enter player name.\n>>>> ");
			playerName = scanner.nextLine().trim();

			if (playerName == null || !playerName.isEmpty())
				break;
			else
				System.out.println("Please enter valid name");
		}
		return playerName;
	}

	public static String getCoordinates(int runNum, Player player) {
		if (runNum == 1) {
			System.out.print(player.getColor() + " Trun, " + player.getName()
					+ ", input your location to move from. (EX: A2)\n>>>> ");
		} else {
			System.out.print(player.getColor() + " Trun, " + player.getName()
					+ ", input you location to move to. (EX: E4)\n>>>> ");
		}
		return scanner.nextLine().trim();
	}

}
