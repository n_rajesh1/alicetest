package com.test.chess.util;

public class ApplicationConstants {
	public static final String CAMEL = "Camel";
	public static final String ELEPHANT = "Elephant";
	public static final String HORSE = "Horse";
	public static final String KING = "King";
	public static final String QUEEN = "Queen";
	public static final String PAWN = "Pawn";

	public static final char SIDE_ALPHABATES[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
	protected static final int SIDE_NUMS[] = { 1, 2, 3, 4, 5, 6, 7, 8 };

	public static final String BLANK = "blank";
	public static final String WHITE = "white";
	public static final String BLACK = "black";
	public static final String BOTTOM = "bottom";
	public static final String TOP = "top";
	public static final String RIGHT = "right";
	public static final String LEFT = "left";

}
