package com.test.chess.pieces;

import com.test.chess.board.Move;
import com.test.chess.board.Square;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationUtils;

public class Elephant extends Piece {

	public Elephant(String color) {
		super(color, ApplicationConstants.ELEPHANT);
		icon = ApplicationUtils.getIcon(color, ApplicationConstants.ELEPHANT);
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String playerColor) {

		String moveDirection;
		int displaceMax;
		moveDirection = getMoveDirection(move);
		if (moveDirection == null) {
			return false;
		}

		if ((moveDirection == ApplicationConstants.RIGHT) || (moveDirection == ApplicationConstants.LEFT)) {
			displaceMax = Math.abs(move.getTo().getX() - move.getFrom().getX());
		} else {
			displaceMax = Math.abs(move.getTo().getY() - move.getFrom().getY());
		}
		return isValidMoveInDir(mat, move, playerColor, moveDirection, displaceMax);
	}

	private boolean isValidMoveInDir(Square[][] mat, Move move, String playerColor, String moveDirection,
			int displaceMax) {
		Square tempSquare;
		for (int displace = 1; displace <= displaceMax; displace++) {
			if (moveDirection == ApplicationConstants.RIGHT || moveDirection == ApplicationConstants.TOP) {
				tempSquare = mat[move.getFrom().getY()][move.getFrom().getX() + displace];

				if (isInValidDisplacement(tempSquare, displaceMax, displace)) {
					return false;
				} else if (isValidDisplacement(playerColor, tempSquare, displaceMax, displace)) {
					return true;
				}
			} else {
				tempSquare = mat[move.getFrom().getY()][move.getFrom().getX() - displace];

				if (isInValidDisplacement(tempSquare, displaceMax, displace)) {
					return false;
				} else if (isValidDisplacement(playerColor, tempSquare, displaceMax, displace)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isInValidDisplacement(Square tempSquare, int displaceMax, int displace) {
		return (tempSquare.getType() != ApplicationConstants.BLANK) && (displace != displaceMax);
	}

	private boolean isValidDisplacement(String playerColor, Square tempSquare, int displaceMax, int displace) {
		return (displace == displaceMax)
				&& ((tempSquare.getType() == ApplicationConstants.BLANK) || (tempSquare.getColor() != playerColor));
	}

	/**
	 * @param move
	 * @return
	 */
	private String getMoveDirection(Move move) {
		String moveDirection;
		if (move.getTo().getY() == move.getFrom().getY()) {
			if (move.getTo().getX() > move.getFrom().getX()) {
				moveDirection = ApplicationConstants.RIGHT;
			} else {
				moveDirection = ApplicationConstants.LEFT;
			}
		} else if (move.getTo().getX() == move.getFrom().getX()) {
			if (move.getTo().getY() > move.getFrom().getY()) {
				moveDirection = ApplicationConstants.BOTTOM;
			} else {
				moveDirection = ApplicationConstants.TOP;
			}
		} else {
			moveDirection = null;
		}
		return moveDirection;
	}

}
