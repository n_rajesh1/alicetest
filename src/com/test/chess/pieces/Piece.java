package com.test.chess.pieces;

import com.test.chess.board.Square;

public abstract class Piece extends Square{
	
	public Piece(String color, String type) {
		super(type);
		this.color = color;
	} 
	
	
}
