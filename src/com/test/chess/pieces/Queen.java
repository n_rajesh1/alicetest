package com.test.chess.pieces;

import com.test.chess.board.Move;
import com.test.chess.board.Square;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationUtils;

public class Queen extends Piece {

	public Queen(String color) {
		super(color, ApplicationConstants.QUEEN);
		icon = ApplicationUtils.getIcon(color, ApplicationConstants.QUEEN);
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String playerColor) {
		String moveDirection;
		String type;

		if (move.getTo().getY() == move.getFrom().getY()) {
			if (move.getTo().getX() > move.getFrom().getX()) {
				moveDirection = ApplicationConstants.RIGHT;
				type = "straight";
			} else {
				moveDirection = ApplicationConstants.LEFT;
				type = "straight";
			}
		} else if (move.getTo().getX() == move.getFrom().getX()) {
			if (move.getTo().getY() > move.getFrom().getY()) {
				moveDirection = ApplicationConstants.BOTTOM;
				type = "straight";
			} else {
				moveDirection = ApplicationConstants.TOP;
				type = "straight";
			}
		} else if (move.getTo().getX() > move.getFrom().getX()) {
			if (move.getTo().getY() < move.getFrom().getY()) {
				moveDirection = "topRite";
				type = "diagonal";
			} else {
				moveDirection = "botRight";
				type = "diagonal";
			}
		} else if (move.getTo().getX() < move.getFrom().getX()) {
			if (move.getTo().getY() < move.getFrom().getY()) {
				moveDirection = "topLeft";
				type = "diagonal";
			} else {
				moveDirection = "botLeft";
				type = "diagonal";
			}
		} else {
			return false;
		}

		Square testSquare;

		if (type == "diagonal") {
			int moveDistance = Math.abs(move.getTo().getX() - move.getFrom().getX());

			for (int diagonalOffset = 1; diagonalOffset <= moveDistance; diagonalOffset++) {

				if (moveDirection == "topRite") {
					testSquare = mat[move.getFrom().getY() - diagonalOffset][move.getFrom().getX() + diagonalOffset];
				} else if (moveDirection == "botRight") {
					testSquare = mat[move.getFrom().getY() + diagonalOffset][move.getFrom().getX() + diagonalOffset];
				} else if (moveDirection == "topLeft") {
					testSquare = mat[move.getFrom().getY() - diagonalOffset][move.getFrom().getX() - diagonalOffset];
				} else {
					testSquare = mat[move.getFrom().getY() + diagonalOffset][move.getFrom().getX() - diagonalOffset];
				}

				if ((testSquare.getType() != ApplicationConstants.BLANK) && (diagonalOffset != moveDistance)) {
					return false;
				} else if ((diagonalOffset == moveDistance) && ((testSquare.getColor() != playerColor)
						|| (testSquare.getType() == ApplicationConstants.BLANK))) {
					return true;
				}
			}
		} else { 
			if ((moveDirection == ApplicationConstants.RIGHT) || (moveDirection == ApplicationConstants.LEFT)) {
				int displaceMax = Math.abs(move.getTo().getX() - move.getFrom().getX());

				for (int displace = 1; displace <= displaceMax; displace++) {
					if (moveDirection == ApplicationConstants.RIGHT) {
						testSquare = mat[move.getFrom().getY()][move.getFrom().getX() + displace];
						if ((testSquare.getType() != ApplicationConstants.BLANK) && (displace != displaceMax)) {
							return false;
						} else if ((displace == displaceMax) && ((testSquare.getType() == ApplicationConstants.BLANK)
								|| (testSquare.getColor() != playerColor))) {
							return true;
						}
					} else {
						testSquare = mat[move.getFrom().getY()][move.getFrom().getX() - displace];
						if ((testSquare.getType() != ApplicationConstants.BLANK) && (displace != displaceMax)) {
							return false;
						} else if ((displace == displaceMax) && ((testSquare.getType() == ApplicationConstants.BLANK)
								|| (testSquare.getColor() != playerColor))) {
							return true;
						}
					}
				}
			} else { 
				int displaceMax = Math.abs(move.getTo().getY() - move.getFrom().getY());

				for (int displace = 1; displace <= displaceMax; displace++) {

					if (moveDirection == ApplicationConstants.TOP) {
						testSquare = mat[move.getFrom().getY() - displace][move.getFrom().getX()];
						if ((testSquare.getType() != ApplicationConstants.BLANK) && (displace != displaceMax)) {
							return false;
						} else if ((displace == displaceMax) && ((testSquare.getType() == ApplicationConstants.BLANK)
								|| (testSquare.getColor() != playerColor))) {
							return true;
						}
					} else {
						testSquare = mat[move.getFrom().getY() + displace][move.getFrom().getX()];
						if ((testSquare.getType() != ApplicationConstants.BLANK) && (displace != displaceMax)) {
							return false;
						} else if ((displace == displaceMax) && ((testSquare.getType() == ApplicationConstants.BLANK)
								|| (testSquare.getColor() != playerColor))) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
