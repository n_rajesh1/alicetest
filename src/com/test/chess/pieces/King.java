package com.test.chess.pieces;

import com.test.chess.board.Move;
import com.test.chess.board.Square;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationUtils;

public class King extends Piece {

	public King(String color) {
		super(color, ApplicationConstants.KING);
		icon = ApplicationUtils.getIcon(color, ApplicationConstants.KING);
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String playerColor) {

		Square toSquare = mat[move.getTo().getY()][move.getTo().getX()];

		for (int moveAwayX = -1; moveAwayX <= 1; moveAwayX++) {
			for (int moveAwayY = -1; moveAwayY <= 1; moveAwayY++) {
				if (isValidCoordinates(move, moveAwayX, moveAwayY)
						&& ((toSquare.getType() != ApplicationConstants.BLANK) && (toSquare.getColor() != playerColor)
								|| (toSquare.getType() == ApplicationConstants.BLANK))) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean isValidCoordinates(Move move, int moveAwayX, int moveAwayY) {
		return move.getTo().getX() == move.getFrom().getX() + moveAwayX
				&& move.getTo().getY() == move.getFrom().getY() + moveAwayY;
	}
}
