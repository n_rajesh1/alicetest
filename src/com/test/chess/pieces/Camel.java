package com.test.chess.pieces;

import com.test.chess.board.Move;
import com.test.chess.board.Square;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationUtils;

public class Camel extends Piece {

	public Camel(String color) {
		super(color, ApplicationConstants.CAMEL);
		icon = ApplicationUtils.getIcon(color, ApplicationConstants.CAMEL);
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String playerColor) {
		int moveDistance = Math.abs(move.getTo().getX() - move.getFrom().getX());
		String moveDirection;
		moveDirection = getMoveDirection(move);
		Square tempSquare;

		for (int diagonalMoveIndex = 1; diagonalMoveIndex <= moveDistance; diagonalMoveIndex++) {

			if (moveDirection == "topRight") {
				tempSquare = mat[move.getFrom().getY() - diagonalMoveIndex][move.getFrom().getX() + diagonalMoveIndex];
			} else if (moveDirection == "botRight") {
				tempSquare = mat[move.getFrom().getY() + diagonalMoveIndex][move.getFrom().getX() + diagonalMoveIndex];
			} else if (moveDirection == "topLeft") {
				tempSquare = mat[move.getFrom().getY() - diagonalMoveIndex][move.getFrom().getX() - diagonalMoveIndex];
			} else {
				tempSquare = mat[move.getFrom().getY() + diagonalMoveIndex][move.getFrom().getX() - diagonalMoveIndex];
			}

			if ((tempSquare.getType() != ApplicationConstants.BLANK) && (diagonalMoveIndex != moveDistance)) {
				return false;
			} else if ((diagonalMoveIndex == moveDistance) && ((tempSquare.getColor() != playerColor)
					|| (tempSquare.getType() == ApplicationConstants.BLANK))) {
				return true;
			}
		}
		return false;
	}

	private String getMoveDirection(Move move) {
		String moveDirection;
		if (move.getTo().getX() > move.getFrom().getX()) {
			if (move.getTo().getY() < move.getFrom().getY()) {
				moveDirection = "topRight";
			} else {
				moveDirection = "botRight";
			}
		} else {
			if (move.getTo().getY() < move.getFrom().getY()) {
				moveDirection = "topLeft";
			} else {
				moveDirection = "botLeft";
			}
		}
		return moveDirection;
	}

}
