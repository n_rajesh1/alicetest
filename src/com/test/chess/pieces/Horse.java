package com.test.chess.pieces;

import com.test.chess.board.Move;
import com.test.chess.board.Square;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationUtils;

public class Horse extends Piece {

	public Horse(String color) {
		super(color, ApplicationConstants.HORSE);
		icon = ApplicationUtils.getIcon(color, ApplicationConstants.HORSE);
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String playerColor) {
		Square toSquare = mat[move.getTo().getY()][move.getTo().getX()];
		boolean isValidToLocation = false;
		for (int xDisplace = -2; xDisplace <= 2; xDisplace++) {
			if (xDisplace != 0 && move.getTo().getX() == move.getFrom().getX() + xDisplace) {

				if (Math.abs(xDisplace) == 1) {
					for (int yDisplace = -2; yDisplace <= 2; yDisplace += 4) {
						isValidToLocation = isValidToLocation(move, isValidToLocation, yDisplace);
					}
				} else {
					for (int yDisplace = -1; yDisplace <= 1; yDisplace += 2) {
						isValidToLocation = isValidToLocation(move, isValidToLocation, yDisplace);
					}
				}
			}
		}
		return isValidToLocation && (toSquare.getType() == ApplicationConstants.BLANK || (toSquare.getColor() != playerColor));
	}



	/**
	 * @param move
	 * @param isValidToLocation
	 * @param yDisplace
	 * @return
	 */
	private boolean isValidToLocation(Move move, boolean isValidToLocation, int yDisplace) {
		if (move.getTo().getY() == move.getFrom().getY() + yDisplace) {
			isValidToLocation = true;
		}
		return isValidToLocation;
	}
}
