package com.test.chess.pieces;

import com.test.chess.board.Move;
import com.test.chess.board.Square;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationUtils;

public class Pawn extends Piece {

	public Pawn(String color) {
		super(color, ApplicationConstants.PAWN);
		icon = ApplicationUtils.getIcon(color, ApplicationConstants.PAWN);
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String playerColor) {

		int moveFwdTwo;
		int moveFwdOne;
		int pawnRowIndex;
		if (playerColor == ApplicationConstants.WHITE) {
			moveFwdTwo = -2;
			moveFwdOne = -1;
			pawnRowIndex = 6;
		} else {
			moveFwdTwo = 2;
			moveFwdOne = 1;
			pawnRowIndex = 1;
		}
		Square toSquare = mat[move.getTo().getY()][move.getTo().getX()];
		if (move.getTo().getY() == move.getFrom().getY() + moveFwdOne) {
			if (isValidFwdOneMove(move, toSquare) || isValidDiagonalMove(move, playerColor, toSquare)) {
				return true;
			}
		} else if (isValidFwdTwoMove(move, moveFwdTwo, pawnRowIndex, toSquare)) {
			return true;
		}

		return false;
	}

	private boolean isValidDiagonalMove(Move move, String playerColor, Square toSquare) {
		return (move.getTo().getX() == move.getFrom().getX() - 1) || (move.getTo().getX() == move.getFrom().getX() + 1)
				&& (toSquare.getType() != ApplicationConstants.BLANK) && (toSquare.getColor() != playerColor);
	}

	private boolean isValidFwdOneMove(Move move, Square toSquare) {
		return (move.getTo().getX() == move.getFrom().getX()) && (toSquare.getType() == ApplicationConstants.BLANK);
	}

	private boolean isValidFwdTwoMove(Move move, int moveFwdTwo, int pawnRowIndex, Square toSquare) {
		return (move.getTo().getY() == move.getFrom().getY() + moveFwdTwo)
				&& (move.getTo().getX() == move.getFrom().getX()) && (toSquare.getType() == "blank")
				&& move.getFrom().getY() == pawnRowIndex;
	}

}
