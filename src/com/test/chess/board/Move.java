package com.test.chess.board;

public class Move {
	private Coordinates from;
	private Coordinates to;
	private boolean valid;
	private boolean check;

	public Move(Coordinates from, Coordinates to) {
		super();
		this.from = from;
		this.to = to;
	}

	public Move(int fromX, int fromY, int toX, int toY) {
		super();
		this.from = new Coordinates(fromX, fromY);
		this.to = new Coordinates(toX, toY);
	}

	public Move() {
		this.valid = true;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public boolean isValid() {
		return this.valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public Coordinates getFrom() {
		return from;
	}

	public void setFrom(Coordinates from) {
		this.from = from;
	}

	public Coordinates getTo() {
		return to;
	}

	public void setTo(Coordinates to) {
		this.to = to;
	}

}
