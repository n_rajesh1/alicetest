package com.test.chess.board;

import com.test.chess.util.ApplicationConstants;

public class ChessGame {
	Player whitePlayer;
	Player blackPlayer;
	Board board;

	public ChessGame(Player whitePlayer, Player blackPlayer, Board board) {
		super();
		this.whitePlayer = whitePlayer;
		this.blackPlayer = blackPlayer;
		this.board = board;
	}

	public void show() {
		this.board.draw();
	}

	public void start() {
		while (true) {
			for (int thisRun = 1; thisRun <= 2; thisRun++) {
				this.show();
				Move currentMove = new Move();

				while (true) {

					if (thisRun == 1) {
						currentMove = this.board.getMove(whitePlayer);
					} else {
						currentMove = this.board.getMove(blackPlayer);
					}

					if (!currentMove.isValid()) {
						System.out.println("Invalid locations. Try again.");
						continue;
					}

					Square fromSquare = this.board.getSquare(currentMove.getFrom());

					if (fromSquare.isValidMove(this.board.getMat(), currentMove, getColor(thisRun))) {

						if (this.board.getSquare(currentMove.getTo()).getType() != ApplicationConstants.KING) {
							this.board.performMove(currentMove);
							if (this.board.isCheckMate(getColor(thisRun))) {
								System.out.println("Check Mate!");
							}
							break;
						} else {
							System.out.println("Kings Position");
						}

					} else {
						System.out.println("Invalid move. Try again.");
					}
				}
			}
		}
	}

	private String getColor(int thisRun) {
		if (thisRun == 1) {
			return ApplicationConstants.WHITE;
		} else {
			return ApplicationConstants.BLACK;
		}
	}
}
