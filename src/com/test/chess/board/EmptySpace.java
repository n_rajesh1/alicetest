package com.test.chess.board;

import com.test.chess.util.ApplicationConstants;

public class EmptySpace extends Square {

	public EmptySpace() {
		super(ApplicationConstants.BLANK);
		icon = "   ";
		color = null;
	}

	public void move(int[] moveToLoc) {
	}

	@Override
	public boolean isValidMove(Square[][] mat, Move move, String plyColor) {
		System.out.println("Can not perform move on empty space");
		return false;
	}

}
