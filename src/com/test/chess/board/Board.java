package com.test.chess.board;

import com.test.chess.pieces.Camel;
import com.test.chess.pieces.Elephant;
import com.test.chess.pieces.Horse;
import com.test.chess.pieces.King;
import com.test.chess.pieces.Pawn;
import com.test.chess.pieces.Queen;
import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationScanner;
import com.test.chess.util.ApplicationUtils;

public class Board {
	private Square mat[][];

	public Board() {
		this.mat = new Square[8][8];
	}

	public Square[][] getMat() {
		return mat;
	}

	public void setMat(Square[][] mat) {
		this.mat = mat;
	}

	public void init() {
		mat[0][0] = new Elephant(ApplicationConstants.BLACK);
		mat[0][1] = new Horse(ApplicationConstants.BLACK);
		mat[0][2] = new Camel(ApplicationConstants.BLACK);
		mat[0][3] = new Queen(ApplicationConstants.BLACK);
		mat[0][4] = new King(ApplicationConstants.BLACK);
		mat[0][5] = new Camel(ApplicationConstants.BLACK);
		mat[0][6] = new Horse(ApplicationConstants.BLACK);
		mat[0][7] = new Elephant(ApplicationConstants.BLACK);

		for (int i = 0; i < 8; i++) {
			mat[1][i] = new Pawn(ApplicationConstants.BLACK);
		}

		for (int i = 2; i < 6; i++) {
			for (int j = 0; j < 8; j++) {
				mat[i][j] = new EmptySpace();
			}
		}

		for (int i = 0; i < 8; i++) {
			mat[6][i] = new Pawn(ApplicationConstants.WHITE);
		}

		mat[7][0] = new Elephant(ApplicationConstants.WHITE);
		mat[7][1] = new Horse(ApplicationConstants.WHITE);
		mat[7][2] = new Camel(ApplicationConstants.WHITE);
		mat[7][3] = new Queen(ApplicationConstants.WHITE);
		mat[7][4] = new King(ApplicationConstants.WHITE);
		mat[7][5] = new Camel(ApplicationConstants.WHITE);
		mat[7][6] = new Horse(ApplicationConstants.WHITE);
		mat[7][7] = new Elephant(ApplicationConstants.WHITE);

	}

	public boolean isCheckMate(String plyColor) {
		for (int kingY = 0; kingY < 8; kingY++) {
			for (int kingX = 0; kingX < 8; kingX++) {
				Square tmpSquare = mat[kingY][kingX];
				if ((tmpSquare.getType() == ApplicationConstants.KING) && (tmpSquare.getColor() != plyColor)) {
					return canKingMove(plyColor, kingY, kingX);
				}
			}
		}
		return false;
	}

	private boolean canKingMove(String plyColor, int kingY, int kingX) {
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				Square kingToSqaure = mat[y][x];

				if ((kingToSqaure.getType() != ApplicationConstants.BLANK) && (kingToSqaure.getColor() == plyColor)) {
					Move checkMove = new Move(new Coordinates(x, y), new Coordinates(kingX, kingY));
					if (kingToSqaure.isValidMove(this.mat, checkMove, plyColor)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void performMove(Move currentMove) {
		Coordinates from = currentMove.getFrom();
		Coordinates to = currentMove.getTo();
		mat[to.getY()][to.getX()] = mat[from.getY()][from.getX()];
		mat[from.getY()][from.getX()] = new EmptySpace();
	}

	public void draw() {
		System.out.print("\n   ");

		for (char i : ApplicationConstants.SIDE_ALPHABATES) {
			System.out.print("  " + i + "  ");
		}
		System.out.print("\n   ");

		for (int i = 0; i < 8; i++) {
			System.out.print(" --- ");
		}

		System.out.print("\n");
		for (int i = 0; i < 8; i++) {
			System.out.print(" " + (8 - i) + " ");

			for (Square j : this.mat[i]) {
				System.out.print("|" + j.getIcon() + "|");
			}
			System.out.print(" " + (8 - i) + " ");

			System.out.print("\n   ");

			for (int j = 0; j < 8; j++) {
				System.out.print(" --- ");
			}
			System.out.print("\n");
		}
		System.out.print("   ");
		for (char i : ApplicationConstants.SIDE_ALPHABATES) {
			System.out.print("  " + i + "  ");
		}
		System.out.print("\n\n");
	}

	public Square getSquare(Coordinates co) {
		return this.mat[co.getY()][co.getX()];
	}

	public Move getMove(Player player) {
		Move currentMove = new Move();
		for (int runNum = 1; runNum <= 2; runNum++) {
			while (true) {
				String moveIn = ApplicationScanner.getCoordinates(runNum, player);

				if (isValidCoordinates(moveIn)) {
					Coordinates co = new Coordinates(
							ApplicationUtils.convertCharToNum(Character.toUpperCase(moveIn.charAt(0))),
							8 - ApplicationUtils.convertCharNumtoNum(moveIn.charAt(1)));
					if (runNum == 1) {
						Square tmpSquare = this.mat[co.getY()][co.getX()];
						if (tmpSquare.getType() == ApplicationConstants.BLANK
								|| tmpSquare.getColor() != player.getColor()) {
							currentMove.setValid(false);
						} else {
							currentMove.setFrom(co);
						}
					} else {
						currentMove.setTo(co);
					}
					break;

				}
				currentMove.setValid(false);
			}
		}
		return currentMove;
	}

	private boolean isValidCoordinates(String moveIn) {
		return (!moveIn.isEmpty() && moveIn.length() <= 2 && !(moveIn.contains(" ") || moveIn.contains("\t")))
				&& (!Character.isDigit(moveIn.charAt(0)) && Character.isDigit(moveIn.charAt(1)))
				&& (ApplicationUtils.convertCharToNum(Character.toUpperCase(moveIn.charAt(0)))) != -1
				&& (ApplicationUtils.convertCharNumtoNum(moveIn.charAt(1))) != -1;
	}
}
