package com.test.chess.board;

public abstract class Square {

	protected String icon;
	public String color;
	public String type;

	public Square(String type) {
		this.type = type;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getColor() {
		return color;
	}

	public String getType() {
		return type;
	}

	public abstract boolean isValidMove(Square[][] mat, Move move, String playerColor);
}
