package com.test.chess.board;

import com.test.chess.util.ApplicationConstants;
import com.test.chess.util.ApplicationScanner;

public class ChessGameStart {

	public static void main(String[] args) {
		System.out.println("****************> CHESS GAME START <****************");
		Board board = new Board();
		board.init();

		Player whitePlayer = new Player(ApplicationScanner.readPlayerName(ApplicationConstants.WHITE),
				ApplicationConstants.WHITE);
		Player blackPlayer = new Player(ApplicationScanner.readPlayerName(ApplicationConstants.BLACK),
				ApplicationConstants.BLACK);

		ChessGame chessGame = new ChessGame(whitePlayer, blackPlayer, board);

		chessGame.show();
		chessGame.start();

	}
}
